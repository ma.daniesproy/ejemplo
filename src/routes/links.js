const express=require('express');
const router=express.Router();
const pool=require('../database');

router.get('/Agregar', (req,res)=>{
    res.render('links/Agregar');
});

router.post('/Agregar',async (req, res) => {
    const {nombre, url, descripcion}=req.body;
    const newLink={
        nombre,
        url,
        descripcion
    };
    //console.log(newLink);
    
    await pool.query('INSERT INTO p set ?', [newLink]); 
    res.send('received');
});

router.get('/',async(req,res)=>{
    const links=await pool.query('SELECT * FROM p');
    res.render('links/listaPV',{links});
});

module.exports=router;