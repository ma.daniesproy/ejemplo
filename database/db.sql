CREATE DATABASE paginaVisit;

USE paginaVisit;

CREATE TABLE persona(
    cve INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(30) NOT NULL,
    paterno VARCHAR(80)  NOT NULL,
    materno VARCHAR(80)  NOT NULL,
    fechaNac DATE not null,
    sexo SET('M','F') NOT NULL,
    telefono VARCHAR(10)  NOT NULL,
    calle VARCHAR(80)  NOT NULL,
    orientacion SET('Norte','Oriente','Poniente','Sur', 'Desconocido')  NOT NULL,
    numeroExt VARCHAR(4)  NOT NULL,
    numeroInt VARCHAR(80)  NOT NULL,
    colonia VARCHAR(25)  NOT NULL
);

CREATE TABLE usuario(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(18) NOT NULL,
    password VARCHAR(30) NOT NULL,
    cve INT NOT NULL,FOREIGN KEY(cve) REFERENCES persona(cve) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE paginaVisitada(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    url TEXT NOT NULL,
    descripcion TEXT NOT NULL,
    cve INT NOT NULL,FOREIGN KEY(id) REFERENCES usuario(id) ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE p(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    url TEXT NOT NULL,
    descripcion TEXT NOT NULL);
